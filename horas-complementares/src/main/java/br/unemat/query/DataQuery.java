/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unemat.query;

import br.unemat.entity.Usuario;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class DataQuery {
    
    EntityManagerFactory emf;
    EntityManager em;

    public DataQuery() {
        emf = Persistence.createEntityManagerFactory("primePU");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        
    }
    
    public boolean loginControl(String cpf, String password){
        try{
            Usuario u = em.createNamedQuery("Usuario.control", Usuario.class).setParameter("cpf", cpf).setParameter("password", password).getSingleResult();
            if(u != null){
                return true;
            }
            return false;
        } catch (Exception e){
            return false;
        }
    }
    
    public void criaUsuario(Usuario u){
        em.persist(u);
        em.getTransaction().commit();
        em.close();
        emf.close();
    }
    
}
