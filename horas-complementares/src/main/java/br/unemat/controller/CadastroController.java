/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unemat.controller;

import br.unemat.entity.Usuario;
import br.unemat.query.DataQuery;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

@ManagedBean(name = "cadastroController")
@SessionScoped
public class CadastroController implements Serializable{
    private String nomeCompleto;
    private String password;
    private String cpf;

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    
    private DataQuery query = new DataQuery();
    
    public void criaUsuario(){
        Usuario u = new Usuario(nomeCompleto, password, cpf);
        query.criaUsuario(u);
        
    }
}
